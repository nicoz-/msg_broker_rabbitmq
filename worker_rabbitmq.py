import pika

# Connessione al message broker RabbitMQ
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Dichiarazione della coda "messaggi"
channel.queue_declare(queue='messaggi')

def callback(ch, method, properties, body):
    print(f"Ricevuto un messaggio: {body}")

channel.basic_consume(queue='messaggi', on_message_callback=callback, auto_ack=True)

print("[*]In attesa di messaggi. Premi CTRL+C per interrompere.")
channel.start_consuming()
