from flask import Flask, render_template, request, redirect, url_for
from datetime import datetime

app = Flask(__name__)

# Nome del file per memorizzare i messaggi
file_messaggi = 'messaggi.txt'

# Funzione per caricare i messaggi da file
def carica_messaggi_da_file():
    try:
        with open(file_messaggi, 'r') as file:
            messaggi = file.read().splitlines()
    except FileNotFoundError:
        messaggi = []
    return messaggi

# Funzione per salvare i messaggi su file
def salva_messaggi_su_file(messaggi):
    with open(file_messaggi, 'w') as file:
        file.write('\n'.join(messaggi))

# Carica i messaggi iniziali dal file
messaggi = carica_messaggi_da_file()

@app.route('/')
def index():
    return render_template('index.html', messaggi=messaggi)

@app.route('/invia_messaggio', methods=['POST'])
def invia_messaggio():
    utente = request.form['utente']
    messaggio = request.form['messaggio']
    
    ora_corrente = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    messaggio_completo = f"[{ora_corrente}] {utente}: {messaggio}"
    
    messaggi.append(messaggio_completo)
    
    # Salva i messaggi aggiornati su file
    salva_messaggi_su_file(messaggi)
    
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(debug=True)
